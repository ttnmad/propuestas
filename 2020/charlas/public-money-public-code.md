---
layout: 2020/post
section: proposals
category: talks
title: Public Money Public Code – Global problems need global solutions!
---

Free Software licences allow sharing of code in any jurisdiction. Solutions developed in one country can be reused and adapted in another one.

## Proposal format

Choose one of these:

-   [x]  Talk (25 minutes)
-   [ ]  Lightning talk (10 minutes)

## Description

Free Software licences allow sharing of code in any jurisdiction. Solutions developed in one country can be reused and adapted in another one.

International development agencies and humanitarian movements can help to contain the spread of COVID-19 in any country around the world with the availability of Free Software solutions.

Already before this crisis hundreds of organisations and tens of thousands of people demanded that publicly financed software developed for the public sector must be made publicly available under Free Software licences. It is now even more important than ever before to tackle this crisis.

Do you want to promote Free Software as well? Then the campaign framework of "Public Money? Public Code!" might be the right choice for you; no matter if you want to do it as an individual or as a group; no matter if you have a small or large time budget.

## Target audience

Everybody.

## Speaker(s)

Alexander Sander, Free Software Foundation Europe - Policy Manager

Alexander has studied politics in Marburg and later has been an MEP Assistant in Brussels for three years and the General Manager of Digitale Gesellschaft e.V. in Berlin for four years. Furthermore he is the founder of NoPNR!, a campaign against the retention of travel data. In 2018 he joined FSFE as Policy Manager.

### Contact(s)

-   Alexander Sander: alex.sander at fsfe dot org
-   GitLab: @Alex.Sander

## Observations

\-

## Conditions

-   [x]  I agree to follow the [code of conduct](https://eslib.re/conducta/) and request this acceptance from the attendees and speakers.
-   [x]  At least one person among those proposing will be present on the day scheduled for the talk.
